# $Id$
# Contributor: Corrado Primier <bardo@aur.archlinux.org>
# Contributor: danst0 <danst0@west.de>
# Maintainer: fauxpark <fauxpark@gmail.com>
# Build order: avr-binutils -> avr-gcc -> avr-libc

_realname=libc
_target=avr

pkgbase=mingw-w64-${_target}-${_realname}
pkgname=${MINGW_PACKAGE_PREFIX}-${_target}-${_realname}
pkgver=2.1.0
pkgrel=1
pkgdesc='The C runtime library for the AVR family of microcontrollers (mingw-w64)'
arch=(any)
mingw_arch=('mingw32' 'mingw64' 'ucrt64' 'clang64')
url='https://savannah.nongnu.org/projects/avr-libc/'
license=(BSD)
groups=("${MINGW_PACKAGE_PREFIX}-${_target}-toolchain")
depends=("${MINGW_PACKAGE_PREFIX}-${_target}-gcc")
makedepends=("${MINGW_PACKAGE_PREFIX}-autotools")
options=('staticlibs' '!distcc' '!ccache' '!strip')
source=(https://download.savannah.gnu.org/releases/${_target}-${_realname}/${_target}-${_realname}-${pkgver}.tar.bz2{,.sig})
sha256sums=('0b84cee5c08b5d5cba67c36125e5aaa85251bc9accfba5773bfa87bc34b654e8'
            'SKIP')
validpgpkeys=('56628323218C669FF578705C7E9EADC3030D34EB') # Joerg Wunsch

_basedir=${_target}-${_realname}-${pkgver}
_builddir=build-${_target}-${_realname}-${CARCH}
_prefix=${MINGW_PREFIX}

prepare() {
  cd "$srcdir"/${_basedir} || exit

  [[ -d "$srcdir"/${_builddir} ]] && rm -rf "$srcdir"/${_builddir}
  mkdir -p "$srcdir"/${_builddir}
}

build() {
  cd "$srcdir"/${_basedir} || exit
  ./bootstrap

  CC='avr-gcc' CCAS='avr-gcc' \
  ../${_basedir}/configure \
    --build=${MINGW_CHOST} \
    --host=${_target} \
    --target=${_target} \
    --prefix=${_prefix} \
    --program-prefix=${_target} \
    --enable-device-lib \
    --disable-doc

  make
}

package() {
  cd "$srcdir"/${_builddir}

  make DESTDIR="$pkgdir" install

  install -Dm644 "$srcdir"/${_basedir}/LICENSE "$pkgdir"${_prefix}/share/licenses/${pkgname}/LICENSE
}
