# Maintainer: Jelle van der Waa <jelle@archlinux.org>
# Contributor: Grey Christoforo <my first name [at] my last name [dot] net>

_curaplugin=curaplugin
_realname=svgtoolpathreader
pkgbase=mingw-w64-${_curaplugin}
pkgname=${MINGW_PACKAGE_PREFIX}-${_curaplugin}_${_realname}-git
pkgver=1.2.0.r6.g9f34396d
pkgrel=2
pkgdesc="Cura plug-in to read SVG files as toolpaths, for debugging printer movements. "
depends=(
  "${MINGW_PACKAGE_PREFIX}-cura"
  "${MINGW_PACKAGE_PREFIX}-python-freetype-py"
  )
makedepends=(git
  "${MINGW_PACKAGE_PREFIX}-cmake"
  "${MINGW_PACKAGE_PREFIX}-ninja"
  )
provides=(${pkgname%-git})
conflicts=(${pkgname%-git})
url='https://github.com/Ghostkeeper/SVGToolpathReader'
license=("LGPL")
groups=('cura')
arch=('any')
source=("${_realname}"::"git+https://github.com/Ghostkeeper/${_realname}.git${_fragment}")
sha256sums=('SKIP')

_basedir=${_realname}
_builddir=build-${_realname}-${pkgver}-${CARCH}
_prefix=${MINGW_PREFIX}

prepare(){
  [[ -d "${_builddir}" ]] && rm -r "${_builddir}" || true
}

pkgver() {
  cd "${_basedir}"
  ( set -o pipefail
    git describe --long --tags --match="[v0-9]*\.*" 2>/dev/null |
      sed 's/\([^-]*-g\)/r\1/;s/\(.*\/\)//;s/[:// -]/./g;s/^[^0-9.]*\.*//g' ||
      printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
  ) &&
    cygpath -m "$(cat .git/objects/info/alternates)" >.git/objects/info/alternates || true  # for view git code only
}

build(){
  mkdir -p ${_builddir}
  cd "${_builddir}"

  MSYS2_ARG_CONV_EXCL="-DCMAKE_INSTALL_PREFIX=" \
    ${MINGW_PREFIX}/bin/cmake \
      -GNinja \
      -DCMAKE_INSTALL_PREFIX=${_prefix} \
      -DCMAKE_BUILD_TYPE=RELEASE \
        ../${_basedir}

  ${MINGW_PREFIX}/bin/cmake --build . ${MAKEFLAGS}
}

check() {
  cd ${_builddir}
#  make test
  true
}

package(){
  cd "${_builddir}"
  DESTDIR="${pkgdir}" ${MINGW_PREFIX}/bin/cmake --build . --target install
#  find ${pkgdir}${_prefix}/ | grep -E "(__pycache__|\.pyc|\.pyo$)" | xargs rm -rf
}
