# Maintainer: George Rawlinson <grawlinson@archlinux.org>
# Contributor: Kyle Keen <keenerd@gmail.com>
# Contributor: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Jose Negron <josenj.arch@mailnull.net>

_realname=sdcc
pkgbase="mingw-w64-${_realname}"
pkgname=${MINGW_PACKAGE_PREFIX}-${_realname}
pkgver=4.4.0-rc2
pkgrel=1
pkgdesc='Retargettable ANSI C compiler (Intel 8051, Maxim 80DS390, Zilog Z80 and the Motorola 68HC08) (mingw-w64)'
arch=('any')
url='https://sdcc.sourceforge.net/'
license=('GPL')
depends=(
	"${MINGW_PACKAGE_PREFIX}-gcc-libs"
	)
makedepends=(
	"${MINGW_PACKAGE_PREFIX}-gputils"
	"${MINGW_PACKAGE_PREFIX}-boost"
	'patchutils'
	'bison'
	'flex'
	)
optdepends=(
	"${MINGW_PACKAGE_PREFIX}-python"
	"${MINGW_PACKAGE_PREFIX}-gputils: PIC Programming"
	)
source=(https://downloads.sourceforge.net/sourceforge/sdcc/${_realname}-src-${pkgver//_/-}.tar.bz2
	'r14312.diff'
	'build-all-mcs51-models.patch'
	)
sha512sums=('7429bfe3d35abd8f3b2c0888dbadf90c997698d0e64c2cbc38be1d25b04dfd34bb34376665513dbe65d31d9ada9f39e150fc9536f075d79dd0d4a908634713ab'
            '092c460f5286a104e95e05b1cdc9a0e88058002e1242ae25ad8e5e51e1e9de94329fde3994b2b5a0455b88088d96a53c1b0e5783f796fbeadc49530647347f02'
            '265982849af52f5d84069ef9dd43f6d768eb46b2ff07e1652c3036d69e59fd366481a4447b6c376d2c74123cf6cf5f08afe957fa18a8038a4b0602f4593238f9')
options=('staticlibs' '!distcc' '!ccache' '!strip' '!emptydirs')

_basedir=${_realname}-${pkgver}
_builddir=build-${_realname}-${pkgver}-${CARCH}
_prefix=${MINGW_PREFIX}

prepare() {
	cd "$srcdir/$_basedir"
#	sed -i 's|CC -E|CC -O2 -E|g' support/sdbinutils/libiberty/configure

	# FS#79070: FTBFS
#	filterdiff -p0 -i 'support/cpp/*' ../r14312.diff | patch -Np0

	# FS#79070: Build all models for mcs51 by default
	patch -p1 -i "$srcdir/build-all-mcs51-models.patch"

	[[ -d ${srcdir}/${_builddir} ]] && rm -rf ${srcdir}/${_builddir}
	true
}

build() {
	mkdir -p "${srcdir}/${_builddir}"
	cd "${srcdir}/${_builddir}"
	
	local PATH="${srcdir}/${_builddir}/bin":"${srcdir}/${_basedir}/sdcc/bin":"${PATH}"

	../${_basedir}/configure \
		--prefix=${_prefix} \
		--includedir=${_prefix}/include/${_realname} \
		--libdir=${_prefix}/lib/${_realname} \
		--libexecdir=${_prefix}/lib/lib \
		--disable-werror \
		--disable-doc \
		--enable-gold \
		--enable-lto \
		--disable-rpath \
		--disable-nls \
		--with-system-zlib \
		--disable-ucsim

#		--with-sysroot=${_prefix}/${_target} \    
#		--oldincludedir=/include \
#		--with-native-system-header-dir=/include \
#		--with-local-prefix=${_prefix} \

	make
}

package() {
	cd "${srcdir}/${_builddir}"
	#	make INSTALL_ROOT="${pkgdir}" DESTDIR="${pkgdir}" install
	make DESTDIR="${pkgdir}" install

	if [ -d "${pkgdir}/lib/lib" ]; then
		mv "${pkgdir}/lib/lib/*" "${pkgdir}/lib/sdcc/"
		rm -rf "${pkgdir}/lib/lib"
	fi

#	sed -i "s|#!/usr/bin/env python|#!/usr/bin/env python3|" "${pkgdir}/${_prefix}/bin/as2gbmap"
	rm -rf "${pkgdir}/share/info"
}
