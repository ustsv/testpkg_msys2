# Maintainer: Oscar Fuentes <ofv@wanadoo.es>

_realname=ctags
pkgbase=mingw-w64-universal-${_realname}-git
pkgname=${MINGW_PACKAGE_PREFIX}-universal-${_realname}-git
pkgver=6.0.20230521.0.r8.gbf1ba55d
pkgrel=1
pkgdesc="A maintained Ctags implementation (mingw-w64)"
arch=('any')
mingw_arch=('mingw32' 'mingw64' 'ucrt64' 'clang64' 'clang32')
provides=("${MINGW_PACKAGE_PREFIX}-${_realname}")
conflicts=("${MINGW_PACKAGE_PREFIX}-${_realname}")
url="https://github.com/universal-${_realname}/${_realname}"
license=("GPL")
depends=("${MINGW_PACKAGE_PREFIX}-gcc-libs"
  "${MINGW_PACKAGE_PREFIX}-jansson"
  "${MINGW_PACKAGE_PREFIX}-libiconv"
  "${MINGW_PACKAGE_PREFIX}-libxml2"
  "${MINGW_PACKAGE_PREFIX}-libyaml")
makedepends=(git
  "${MINGW_PACKAGE_PREFIX}-cc"
  "${MINGW_PACKAGE_PREFIX}-python-sphinx"
  "${MINGW_PACKAGE_PREFIX}-autotools")
options=('staticlibs' 'strip')
#_commit="#commit=c81079788bfe650190db8ac9df80d63d2bc160aa"
source=("git+https://github.com/universal-${_realname}/${_realname}.git${_commit}")
sha256sums=('SKIP')

pkgver() {
  cd ${srcdir}/${_realname}
  ( set -o pipefail
    git describe --long --tags --match="[pv0-9]*\.*" 2>/dev/null |
      sed 's/\([^-]*-g\)/r\1/;s/\(.*\/\)//;s/[:// -]/./g;s/^[^0-9.]*\.*//g' ||
      printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
  ) && cygpath -m "$(cat .git/objects/info/alternates)" >.git/objects/info/alternates || true  # for view git code only
}

prepare() {
  cd ${srcdir}/${_realname}
  ./autogen.sh
}

build() {
  [[ -d ${srcdir}/build-${MINGW_CHOST} ]] && rm -rf ${srcdir}/build-${MINGW_CHOST}
  cp -rf ${_realname} ${srcdir}/build-${MINGW_CHOST}
  cd ${srcdir}/build-${MINGW_CHOST}
  ../${_realname}/configure \
    --prefix=${MINGW_PREFIX} \
    --build=${MINGW_CHOST} \
    --host=${MINGW_CHOST} \
    --target=${MINGW_CHOST} \
    --disable-etags \
    --disable-external-sort

  make
  make -C docs html man
}

package() {
  cd "${srcdir}/build-${MINGW_CHOST}"
  make prefix="${pkgdir}${MINGW_PREFIX}" install

  mkdir -p ${pkgdir}${MINGW_PREFIX}/share/doc/universal-${_realname}
  cp -rf docs/_build/html ${pkgdir}${MINGW_PREFIX}/share/doc/universal-${_realname}
  rm -rf ${pkgdir}${MINGW_PREFIX}/share/doc/universal-${_realname}/html/sources

  mkdir -p ${pkgdir}${MINGW_PREFIX}/share/man/man1
  cp -rf docs/_build/man ${pkgdir}${MINGW_PREFIX}/share/man/man1

  install -Dm0644 COPYING ${pkgdir}${MINGW_PREFIX}/share/licenses/universal-${_realname}/COPYING
}
